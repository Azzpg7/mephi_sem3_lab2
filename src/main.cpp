#include <bits/stdc++.h>

#include "btree.h"
#include "sequence.h"
#include "set.h"
#include "sparse_vector.h"

int main() {
  auto b = new BTree<int>;
  b->Insert(3);
  b->Insert(4);
  b->Insert(5);
  b->Insert(1);
  b->Insert(2);
  b->Insert(9);
  b->Insert(10);
  std::cout << "\nActually has: 1 2 3 4 5 9 10\n";
  std::cout << "has 1: " << b->Contains(1) << '\n';
  std::cout << "has 2: " << b->Contains(2) << '\n';
  std::cout << "has 3: " << b->Contains(3) << '\n';
  std::cout << "has 4: " << b->Contains(4) << '\n';
  std::cout << "has 5: " << b->Contains(5) << '\n';
  std::cout << "has 9: " << b->Contains(9) << '\n';
  std::cout << "has 10: " << b->Contains(10) << '\n';

  std::cout << "\nIterating:\n";
  for (auto it = b->begin(); it.HasNext(); it.Next())
    std::cout << it.Get() << ' ';
  std::cout << std::endl;

  b->Erase(1);
  b->Erase(2);
  b->Erase(3);
  b->Erase(4);
  b->Erase(5);
  b->Erase(9);
  b->Erase(10);
  b->Erase(11);

  std::cout << "\nAfter deletion:\n";
  std::cout << "has 1: " << b->Contains(1) << '\n';
  std::cout << "has 2: " << b->Contains(2) << '\n';
  std::cout << "has 3: " << b->Contains(3) << '\n';
  std::cout << "has 4: " << b->Contains(4) << '\n';
  std::cout << "has 5: " << b->Contains(5) << '\n';
  std::cout << "has 9: " << b->Contains(9) << '\n';
  std::cout << "has 10: " << b->Contains(10) << '\n';

  std::cout << "\n\nSET\n";
  auto s = new BTreeSet<int>();
  s->Insert(3);
  s->Insert(4);
  s->Insert(5);
  s->Insert(1);
  s->Insert(2);
  s->Insert(9);
  s->Insert(10);
  s->Insert(17);
  s->Insert(-2);
  std::cout << "\nActually has: 1 2 3 4 5 9 10\n";
  std::cout << "has 1: " << s->Contains(1) << '\n';
  std::cout << "has 2: " << s->Contains(2) << '\n';
  std::cout << "has 3: " << s->Contains(3) << '\n';
  std::cout << "has 4: " << s->Contains(4) << '\n';
  std::cout << "has 5: " << s->Contains(5) << '\n';
  std::cout << "has 9: " << s->Contains(9) << '\n';
  std::cout << "has 10: " << s->Contains(10) << '\n';

  std::cout << "\nIterating:\n";
  for (auto it = s->begin(); it.HasNext(); it.Next())
    std::cout << it.Get() << ' ';
  std::cout << std::endl;

  s->Erase(1);
  s->Erase(2);
  s->Erase(3);
  s->Erase(4);
  s->Erase(5);
  s->Erase(9);
  s->Erase(10);
  s->Erase(11);

  std::cout << "\nAfter deletion:\n";
  std::cout << "has 1: " << s->Contains(1) << '\n';
  std::cout << "has 2: " << s->Contains(2) << '\n';
  std::cout << "has 3: " << s->Contains(3) << '\n';
  std::cout << "has 4: " << s->Contains(4) << '\n';
  std::cout << "has 5: " << s->Contains(5) << '\n';
  std::cout << "has 9: " << s->Contains(9) << '\n';
  std::cout << "has 10: " << s->Contains(10) << '\n';

  std::cout << "\nIterating:\n";
  for (auto it = s->begin(); it.HasNext(); it.Next())
    std::cout << it.Get() << ' ';
  std::cout << std::endl;

  Sequence<int> tmp;
  tmp.Append(0);
  tmp.Append(0);
  tmp.Append(1);
  tmp.Append(-4);
  tmp.Append(0);
  tmp.Append(0);
  tmp.Append(7);
  tmp.Append(0);
  tmp.Append(5);
  auto sv1 = new SparseVector<int>(tmp);
  sv1->Print();
  auto sv2 = new SparseVector<int>(*sv1);
  sv2->Print();

  SparseVector<int> nsv = sv1->Add(*sv2);
  nsv.Print();

  SparseVector<int> nnsv = nsv.Multiply(-3);
  nnsv.Print();

  Sequence<int> tmp2;
  tmp2.Append(0);
  tmp2.Append(0);
  tmp2.Append(-6);
  tmp2.Append(24);
  tmp2.Append(0);
  tmp2.Append(0);
  tmp2.Append(-42);
  tmp2.Append(0);
  tmp2.Append(-30);
  auto to_comp = new SparseVector<int>(tmp2);
  if (*to_comp == nnsv)
    std::cout << "HAHA\n";
  else
    std::cout << "HEHE\n";

  return 0;
}
