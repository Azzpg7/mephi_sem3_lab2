#ifndef BTREE
#define BTREE

#include <bits/stdc++.h>

#include <stdexcept>

#include "sequence.h"

template <typename T>
class BTree {
 private:
  struct Node {
    size_t size;
    Sequence<T> keys;
    Sequence<Node *> children;
    bool leaf;

    Node(size_t _degree, bool _leaf = false)
        : size(0),
          leaf(_leaf),
          keys((_degree << 1) - 1),
          children(_degree << 1) {}

  };

  size_t degree;
  Node *root;

  void insert_non_full(Node *node, T value) {
    int ind = node->size - 1;
    if (node->leaf) {
      while (ind >= 0 && value < node->keys[ind]) {
        node->keys[ind + 1] = node->keys[ind];
        --ind;
      }
      node->keys[ind + 1] = value;
      node->size++;
    } else {
      while (ind >= 0 && value < node->keys[ind]) --ind;
      ++ind;
      // std::cout << "HEHE" << value << std::endl;
      if (node->children[ind]->size == (degree << 1) - 1) {
        split_child(node, ind);
        if (value > node->keys[ind]) ++ind;
      }
      insert_non_full(node->children[ind], value);
    }
  }

  void split_child(Node *node, size_t ind) {
    Node *left = node->children[ind];
    Node *right = new Node(degree);
    right->leaf = left->leaf;
    right->size = degree - 1;
    for (int i = 0; i < degree - 1; ++i)
      right->keys[i] = left->keys[i + degree];
    if (!left->leaf)
      for (int i = 0; i < degree; ++i)
        right->children[i] = left->children[i + degree];
    left->size = degree - 1;
    for (int i = node->size; i > ind; --i)
      node->children[i + 1] = node->children[i];
    node->children[ind + 1] = right;
    if (node->size)
      for (int i = node->size - 1; i >= ind; --i)
        node->keys[i + 1] = node->keys[i];
    node->size += 1;
    node->keys[ind] = left->keys[degree - 1];
  }

  Node *find(Node *node, const T &value) {
    size_t ind = 0;
    while (ind < node->size && node->keys[ind] < value) ++ind;
    if (ind < node->size && value == node->keys[ind]) return node;
    if (node->leaf)
      return nullptr;
    else
      return find(node->children[ind], value);
  }

  void erase(Node *&node, const T &value) {
    size_t ind = 0;
    while (ind < node->size && node->keys[ind] < value) ++ind;
    if (ind < node->size && node->keys[ind] == value) {
      if (node->leaf)
        erase_from_leaf(node, ind);
      else
        erase_from_non_leaf(node, ind);
    } else {
      if (node->leaf) return;

      bool flag = (ind == node->size) ? true : false;
      if (node->children[ind]->size < degree) fill(node, ind);

      if (flag && ind > node->size)
        erase(node->children[ind - 1], value);
      else
        erase(node->children[ind], value);

      if (node->size == 0) node = node->children[0];
    }
    return;
  }

  void erase_from_leaf(Node *node, size_t ind) {
    for (size_t i = ind + 1; i < node->size; ++i)
      node->keys[i - 1] = node->keys[i];
    node->size--;
    return;
  }

  void erase_from_non_leaf(Node *node, size_t ind) {
    T value = node->keys[ind];
    if (node->children[ind]->size >= degree) {
      T pred = getPredecessor(node, ind);
      node->keys[ind] = pred;
      erase(node->children[ind], pred);
    } else if (node->children[ind + 1]->size >= degree) {
      T succ = getSuccessor(node, ind);
      node->keys[ind] = succ;
      erase(node->children[ind + 1], succ);
    } else {
      merge(node, ind);
      erase(node->children[ind], value);
    }
    return;
  }

  T getPredecessor(Node *node, size_t ind) {
    Node *cur = node->children[ind];
    while (!cur->leaf) cur = cur->children[cur->size];
    return cur->keys[cur->size - 1];
  }

  T getSuccessor(Node *node, size_t ind) {
    Node *cur = node->children[ind + 1];
    while (!cur->leaf) cur = cur->children[0];
    return cur->keys[0];
  }

  void fill(Node *node, size_t ind) {
    if (ind != 0 && node->children[ind - 1]->size >= degree)
      borrow_from_prev(node, ind);
    else if (ind != node->size && node->children[ind + 1]->size >= degree)
      borrow_from_next(node, ind);
    else {
      if (ind != node->size)  // Here we have empty root after merge
        merge(node, ind);
      else
        merge(node, ind - 1);
    }
    return;
  };

  void borrow_from_prev(Node *node, size_t ind) {
    Node *child = node->children[ind];
    Node *sibling = node->children[ind - 1];
    for (size_t i = child->size - 1; i >= 0; --i)
      child->keys[i + 1] = child->keys[i];
    if (!child->leaf)
      for (size_t i = child->size; i >= 0; --i)
        child->children[i + 1] = child->children[i];

    child->keys[0] = node->keys[ind - 1];
    if (!child->leaf) child->children[0] = sibling->children[sibling->size];
    node->keys[ind - 1] = sibling->keys[sibling->size - 1];

    child->size++;
    sibling->size--;

    return;
  }

  void borrow_from_next(Node *node, size_t ind) {
    Node *child = node->children[ind];
    Node *sibling = node->children[ind + 1];

    child->keys[child->size] = node->keys[ind];

    if (!child->leaf) child->children[child->size + 1] = sibling->children[0];
    node->keys[ind] = sibling->keys[0];

    for (size_t i = 1; i < sibling->size; ++i)
      sibling->keys[i - 1] = sibling->keys[i];
    if (!sibling->leaf)
      for (size_t i = 1; i <= sibling->size; ++i)
        sibling->children[i - 1] = sibling->children[i];

    child->size++;
    sibling->size--;

    return;
  }

  void merge(Node *node, size_t ind) {
    Node *child = node->children[ind];
    Node *sibling = node->children[ind + 1];

    child->keys[degree - 1] = node->keys[ind];

    for (size_t i = 0; i < sibling->size; ++i)
      child->keys[i + degree] = sibling->keys[i];
    if (!child->leaf)
      for (size_t i = 0; i <= sibling->size; ++i)
        child->children[i + degree] = sibling->children[i];

    for (size_t i = ind + 1; i < node->size; ++i)
      node->keys[i - 1] = node->keys[i];

    for (size_t i = ind + 2; i <= node->size; ++i)
      node->children[i - 1] = node->children[i];

    child->size += sibling->size + 1;
    node->size--;

    delete sibling;
    return;
  }

  void fill_traversal(Node *node, Sequence<T> &traversal) {
    if (node->leaf) {
      for (int i = 0; i < node->size; ++i) traversal.Append(node->keys[i]);
      return;
    }
    int ind = 0;
    while (ind < node->size) {
      fill_traversal(node->children[ind], traversal);
      traversal.Append(node->keys[ind]);
      ++ind;
    }
    fill_traversal(node->children[ind], traversal);
  }

 public:
  BTree<T>(size_t _degree = 3) : degree(_degree), root(nullptr) {}

  class Iterator {
   private:
    int ind;
    Sequence<T> traversal;

   public:
    Iterator() : ind(0), traversal() {}
    Iterator(const Sequence<T> &_t) : ind(0), traversal(_t) {}

    const T &Get() { return traversal[ind]; }

    bool HasNext() {
      if (ind < traversal.Size())
        return true;
      else
        return false;
    }

    void Next() {
      if (ind < traversal.Size())
        ++ind;
      else
        throw std::out_of_range("It is the end of iterator (:");
    }
  };

  Iterator begin() {
    Sequence<T> traversal;
    fill_traversal(root, traversal);
    return Iterator(traversal);
  }

  void Insert(const T &value) {
    if (!root) root = new Node(degree, true);
    if (root->size == (degree << 1) - 1) {
      Node *node = new Node(degree);
      node->children[0] = root;
      root = node;
      split_child(root, 0);
    }
    insert_non_full(root, value);
  }

  bool Contains(const T &value) {
    if (!root) return false;
    return find(root, value);
  }

  void Erase(const T &value) {
    if (!root) return;
    erase(root, value);
  }

};

#endif
