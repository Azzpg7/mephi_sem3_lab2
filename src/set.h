#ifndef BTREE_SET
#define BTREE_SET

#include "btree.h"

template <typename T>
class ISet {
 public:
  virtual void Insert(const T& value) = 0;
  virtual bool Contains(const T& value) = 0;
  virtual void Erase(const T& value) = 0;
};

template <typename T>
class BTreeSet : public ISet<T> {
 private:
  BTree<T> btree;

 public:
  BTreeSet() : btree(10) {}

  class Iterator {
   private:
    BTreeSet<T>* const owner;
    typename BTree<T>::Iterator it;

   public:
    Iterator(BTreeSet<T>& owner) : owner(&owner) { it = owner.btree.begin(); }

    const T& Get() { return it.Get(); }
    bool HasNext() { return it.HasNext(); }
    void Next() { it.Next(); }
  };

  Iterator begin() { return Iterator(*this); }
  void Insert(const T& value) override { btree.Insert(value); }
  bool Contains(const T& value) override { return btree.Contains(value); }
  void Erase(const T& value) override { btree.Erase(value); }
};

#endif
