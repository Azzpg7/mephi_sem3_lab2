#ifndef SPARSE_VECTOR
#define SPARSE_VECTOR

#include "set.h"

template <typename T>
class SparseVector {
 private:
  BTreeSet<std::pair<size_t, T>> bs;

 public:
  SparseVector() : bs() {}
  SparseVector(const SparseVector<T>& other) : bs(other.bs) {}
  SparseVector(const Sequence<T>& seq) : bs() {
    for (size_t i = 0; i < seq.Size(); ++i)
      if (seq[i] != T(0)) bs.Insert({i, seq[i]});
  }

  bool operator==(SparseVector<T>& other) {
    auto other_it = other.bs.begin();
    auto it = bs.begin();
    while (it.HasNext() && other_it.HasNext()) {
      if (it.Get() != other_it.Get()) return false;
      it.Next();
      other_it.Next();
    }
    if (it.HasNext() || other_it.HasNext()) return false;
    return true;
  }

  void Print() {
    std::cout << "Sparse Vector:\n";
    for (auto it = bs.begin(); it.HasNext(); it.Next())
      std::cout << "pos = " << it.Get().first << ", val = " << it.Get().second
                << '\n';
    std::cout << "======\n";
  }

  SparseVector<T> Add(SparseVector<T>& other) {
    Sequence<T> seq;
    int cur_ind = 0;

    auto it = bs.begin();
    auto other_it = other.bs.begin();

    std::pair<size_t, T> el, other_el;
    int insert_ind;
    T value;
    while (it.HasNext() && other_it.HasNext()) {
      el = it.Get();
      other_el = other_it.Get();
      if (el.first < other_el.first) {
        insert_ind = el.first;
        value = el.second;
        it.Next();
        el = it.Get();
      } else if (other_el.first < el.first) {
        insert_ind = other_el.first;
        value = other_el.second;
        other_it.Next();
        other_el = other_it.Get();
      } else {
        insert_ind = el.first;
        value = el.second + other_el.second;
        it.Next();
        other_it.Next();
      }
      while (cur_ind < insert_ind) {
        seq.Append(T(0));
        ++cur_ind;
      }
      seq.Append(value);
      cur_ind++;
    }

    while (it.HasNext()) {
      el = it.Get();
      insert_ind = el.first;
      value = el.second;
      while (cur_ind < insert_ind) {
        seq.Append(T(0));
        ++cur_ind;
      }
      seq.Append(value);
      cur_ind++;
    }
    while (other_it.HasNext()) {
      other_el = other_it.Get();
      insert_ind = other_el.first;
      value = other_el.second;
      while (cur_ind < insert_ind) {
        seq.Append(T(0));
        ++cur_ind;
      }
      seq.Append(value);
      cur_ind++;
    }

    return SparseVector(seq);
  }

  SparseVector<T> Multiply(const T& c) {
    Sequence<T> seq;
    int cur_ind = 0;

    auto it = bs.begin();

    std::pair<size_t, T> el;
    int insert_ind;
    T value;
    while (it.HasNext()) {
      el = it.Get();
      insert_ind = el.first;
      value = el.second;
      while (cur_ind < insert_ind) {
        seq.Append(T(0));
        ++cur_ind;
      }
      seq.Append(value * c);
      it.Next();
      cur_ind++;
    }

    while (it.HasNext()) {
      el = it.Get();
      insert_ind = el.first;
      value = el.second;
      while (cur_ind < insert_ind) {
        seq.Append(T(0));
        ++cur_ind;
      }
      seq.Append(value);
      cur_ind++;
    }

    return SparseVector(seq);
  }
};

#endif
