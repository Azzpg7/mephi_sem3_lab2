#define CATCH_CONFIG_MAIN
#include "../src/set.h"
#include "../src/sparse_vector.h"
#include "catch.h"

TEST_CASE("set") {
  SECTION("check contains()") {
    BTreeSet<int> bs;
    bs.Insert(2);
    REQUIRE(bs.Contains(2));
  }

  SECTION("check negative contains()") {
    BTreeSet<int> bs;
    bs.Insert(2);
    bs.Erase(2);
    REQUIRE(!bs.Contains(2));
  }

  SECTION("check negative contains() after double erase()") {
    BTreeSet<int> bs;
    bs.Insert(2);
    bs.Erase(2);
    bs.Erase(2);
    REQUIRE(!bs.Contains(2));
  }
}

TEST_CASE("sparse vector") {
  SECTION("Addition of two vectors") {
    Sequence<int> tmp1;
    tmp1.Append(1);
    tmp1.Append(0);
    tmp1.Append(1);
    tmp1.Append(-4);
    tmp1.Append(0);
    tmp1.Append(8);
    tmp1.Append(7);
    tmp1.Append(0);
    tmp1.Append(5);
    SparseVector<int> sv1(tmp1);

    Sequence<int> tmp2;
    tmp2.Append(0);
    tmp2.Append(0);
    tmp2.Append(-3);
    tmp2.Append(-4);
    tmp2.Append(0);
    tmp2.Append(0);
    tmp2.Append(2);
    tmp2.Append(0);
    tmp2.Append(5);
    SparseVector<int> sv2(tmp2);

    Sequence<int> tmp_res;
    tmp_res.Append(1);
    tmp_res.Append(0);
    tmp_res.Append(-2);
    tmp_res.Append(-8);
    tmp_res.Append(0);
    tmp_res.Append(8);
    tmp_res.Append(9);
    tmp_res.Append(0);
    tmp_res.Append(10);
    SparseVector<int> sv_res(tmp_res);

    auto sv_real_res = sv1.Add(sv2);

    assert(sv_real_res == sv_res);
    REQUIRE(1 == 1);
  }
  SECTION("Multipply vector on int") {
    Sequence<int> tmp;
    tmp.Append(1);
    tmp.Append(0);
    tmp.Append(1);
    tmp.Append(-4);
    tmp.Append(0);
    tmp.Append(8);
    tmp.Append(7);
    tmp.Append(0);
    tmp.Append(5);
    SparseVector<int> sv(tmp);

    Sequence<int> tmp_res;
    tmp_res.Append(-7);
    tmp_res.Append(0);
    tmp_res.Append(-7);
    tmp_res.Append(28);
    tmp_res.Append(0);
    tmp_res.Append(-56);
    tmp_res.Append(-49);
    tmp_res.Append(0);
    tmp_res.Append(-35);
    SparseVector<int> sv_res(tmp_res);

    auto sv_real_res = sv.Multiply(-7);

    assert(sv_real_res == sv_res);
    REQUIRE(1 == 1);
  }
}
